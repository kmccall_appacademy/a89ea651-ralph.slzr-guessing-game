# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

  def guessing_game
    answers = []
    rand_num = rand(1..100)

    while true
      print 'guess a number'
      current = gets.to_i
      answers << current
        if current == rand_num
          puts "Congratulations! #{current} is the correct number!"
          break
        elsif current < rand_num
          puts "#{current} is too low"
        elsif current > rand_num
          puts "#{current} is too high"
        end
    end

        puts "It took you " + answers.length.to_s + " tries."
  end

# write a file_shuffler program that: *prompts the user for a file name * reads that file*
# shuffles the lines * saves it to the file "{input_name}-shuffled.txt"

# first ask for the file name
# then save that file as an array of separate lines
# shuffle the array
# join the array and save to a new file

def file_shuffler
    contents = []
    puts 'Which file would you like shuffled?'
    input = gets.chomp
    File.foreach(input) do |line|
      each_line = line.chomp
      contents << each_line
    end

    shuffled = contents.shuffle.join("\n")
    File.open("#{input}-shuffled.txt", "w") {|f| f.print shuffled}
end
